#ifndef EXECUTIVE_H
#define EXECUTIVE_H

#include <pthread.h>

void richiesta_task_ap();

void* ap_task_handler(void* arg);

void* p_task_handler(void* arg);

void* executive(void* arg);

void threads_create();

//la politica utilizzata per la deadline miss è lasciar terminare il thread WORKING e abortire i PENDING.
void gestione_deadline();

void destroy();

#endif
