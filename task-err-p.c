#include "task.h"
#include <stdlib.h>
#include <sys/time.h>
#include "busy_wait.h"

/* Lunghezza dell'iperperiodo */
#define H_PERIOD_ 20

/* Numero di frame */
#define NUM_FRAMES_ 5

/* Numero di task */
#define NUM_P_TASKS_ 5

/* Quanto di tempo che settiamo */
#define QUANTO_TEMPORALE 10	

/* Tempi esecuzione task*/

#define epsilon 4 	//stima del tempo d'esecuzione dell'executive

void task1_code();
void task2_code();
void task31_code();
void task32_code();
void task33_code();
void ap_task_code();


int aperiodic_index;
int tempo_task_1 = 8;
int tempo_task_2 = 19;
int tempo_task_31 = 7;
int tempo_task_32 = 26;
int tempo_task_33 = 8;

/* Questo inizializza i dati globali */
const unsigned int H_PERIOD = H_PERIOD_;
const unsigned int NUM_FRAMES = NUM_FRAMES_;
const unsigned int NUM_P_TASKS = NUM_P_TASKS_;

task_routine P_TASKS[NUM_P_TASKS_];
task_routine AP_TASK;
int * SCHEDULE[NUM_FRAMES_];
int SLACK[NUM_FRAMES_];

void task_init()
{
	/* Inizializzazione di P_TASKS[] */
	P_TASKS[0] = task1_code;
	P_TASKS[1] = task2_code;
	P_TASKS[2] = task31_code;
	P_TASKS[3] = task32_code;
	P_TASKS[4] = task33_code;

	/* Inizializzazione di AP_TASK */
	AP_TASK = ap_task_code;

	/* Inizializzazione di SCHEDULE e SLACK */

	/* frame 0 */
	SCHEDULE[0] = (int *) malloc( sizeof( int ) * 4 );
	SCHEDULE[0][0] = 0;
	SCHEDULE[0][1] = 1;
	SCHEDULE[0][2] = 2;
	SCHEDULE[0][3] = -1;
	SLACK[0] = (( H_PERIOD * QUANTO_TEMPORALE / NUM_FRAMES ) - (tempo_task_1 + tempo_task_2 + tempo_task_31 +epsilon)); 

	/* frame 1 */
	SCHEDULE[1] = (int *) malloc( sizeof( int ) * 3 );
	SCHEDULE[1][0] = 0;
	SCHEDULE[1][1] = 3;
	SCHEDULE[1][2] = -1;
	SLACK[1] = (( H_PERIOD * QUANTO_TEMPORALE / NUM_FRAMES ) - (tempo_task_1 + tempo_task_32 +epsilon)); 

	/* frame 2 */
	SCHEDULE[2] = (int *) malloc( sizeof( int ) * 3 );
	SCHEDULE[2][0] = 0;
	SCHEDULE[2][1] = 1;
	SCHEDULE[2][2] = -1;	
	SLACK[2] = (( H_PERIOD * QUANTO_TEMPORALE / NUM_FRAMES ) - (tempo_task_1 + tempo_task_2 +epsilon)); 

	/* frame 3 */
	SCHEDULE[3] = (int *) malloc( sizeof( int ) * 3 );
	SCHEDULE[3][0] = 0;
	SCHEDULE[3][1] = 1;
	SCHEDULE[3][2] = -1;
	SLACK[3] = (( H_PERIOD * QUANTO_TEMPORALE / NUM_FRAMES ) - (tempo_task_1 + tempo_task_2 +epsilon)); 

	/* frame 4 */
	SCHEDULE[4] = (int *) malloc( sizeof( int ) * 4 );
	SCHEDULE[4][0] = 0;
	SCHEDULE[4][1] = 1;
	SCHEDULE[4][2] = 4;
	SCHEDULE[4][3] = -1;
	SLACK[4] = (( H_PERIOD * QUANTO_TEMPORALE / NUM_FRAMES ) - (tempo_task_1 + tempo_task_2 + tempo_task_33 +epsilon));


	busy_wait_init();
	aperiodic_index = 0;
}

void task_destroy()
{	
	int i;
	for ( i = 0; i < NUM_FRAMES; ++i ) {
		free( SCHEDULE[i] );
	}
}


void task1_code()
{
	//durata: 8
	busy_wait(tempo_task_1);

	//Ogni 6 esecuzioni allungo di poco la durata del task 1
	if ( aperiodic_index == 0 ){
		
		busy_wait(8);

	}
	aperiodic_index = aperiodic_index+1;
	aperiodic_index = aperiodic_index % 6;

}

void task2_code()
{
	//durata: 19
	busy_wait(tempo_task_2);
}

void task31_code()
{
	//durata: 7
	busy_wait(tempo_task_31);
}

void task32_code()
{
	//durata: 26
	busy_wait(tempo_task_32);
}

void task33_code()
{
	//durata: 8
	busy_wait(tempo_task_33);
}

void ap_task_code()
{
	busy_wait(10);
}
