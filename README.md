## Sistema di scheduling real time

Il programma è stato scritto in C, implementato nel sistema operativo GNU/Linux,
con l'utilizzo delle API POSIX per gestire la programmazione multithread.

Consente di porre in esecuzione un insieme di task periodici con un approccio clock driven,
utilizzando un algoritmo di scheduling statico.

Il sistema inoltre è in grado di eseguire task aperiodici che arrivano
casualmente e di eseguirli o meno a seconda della priorità e della fattibilità
di tale operazione.

### La struttura del progetto

* __taskOk__ è il file che contiene la schedule senza errori con la gestione del task aperiodico.
* __task-err-p__ è il file che contiene la schedule sbagliata: ogni sei esecuzioni allunga la durata del task 1 facendo andare, in alcuni casi, in deadline miss l’ultimo task di quel frame.
* __task-err-a__ è il file che contiene la schedule in cui il task aperiodico viene chiamato ogni 3 frame, inoltre ne viene aumentata la durata. Quando arriva un task aperiodico in un frame, abbiamo scelto come sua deadline non il rilascio di se stesso, ma il termine del frame in cui viene rilasciato. Se ad esempio ho 5 frame e viene rilasciato nel frame 2, la sua deadline sarà la fine del frame due (al prossimo periodo).
* __task-err-big__ è il file in cui ogni sei esecuzioni viene aumentato il tempo del task 1 (per la precisione diventa più lungo della durata del frame). Quindi tutti i task successivi non vengono eseguiti al frame successivo per politica scelta.