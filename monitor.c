#include <pthread.h>
#include <stdlib.h>

#include "monitor.h"
#include "task.h"


void set_stato(task_monitor* m, stato_task s, int task_index){
	pthread_mutex_lock(&m->mutex);
	m->stato[task_index] = s;
	pthread_mutex_unlock(&m->mutex);
}

stato_task get_stato(task_monitor* m, int task_index){
	stato_task s;	
	pthread_mutex_lock(&m->mutex);
	s = m->stato[task_index];
	pthread_mutex_unlock(&m->mutex);
	return s;
}


void init_monitor(task_monitor* m, int num_tasks){
	int i;
	m->cond = malloc(sizeof(pthread_cond_t) * num_tasks);
	m->stato = malloc(sizeof(stato_task) * num_tasks);
	pthread_mutex_init(&m->mutex, NULL);

	for(i=0;i<num_tasks;i++){
		pthread_cond_init(&m->cond[i], NULL);
	}
}

void wait_monitor(task_monitor* m, int task_index){
	pthread_mutex_lock(&m->mutex);
	m->stato[task_index] = IDLE;
	while(m->stato[task_index] != PENDING){
		pthread_cond_wait(&m->cond[task_index], &m->mutex);
	}	
	m->stato[task_index] = WORKING;
	pthread_mutex_unlock(&m->mutex);
}

void monitor_destroy(task_monitor* m, int num_tasks){
	int i;
	for(i=0;i<num_tasks;i++){
		pthread_cond_destroy(&m->cond[i]);
	}
	free(m->cond);
	free(m->stato);
	pthread_mutex_destroy(&m->mutex);
	free(m);
}
