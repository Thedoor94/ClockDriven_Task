#ifndef MONITOR_H
#define MONITOR_H

#include <pthread.h>

// i 3 stati in cui può trovarsi un task
typedef enum {IDLE, PENDING, WORKING} stato_task;

//Struttura dedicata al monitor
typedef struct {

	pthread_mutex_t mutex;	// Mutex riservato alle operazioni di lettura/scrittura sullo stato
	pthread_cond_t* cond;	// Vettore di variabili condizione, una per task
	stato_task* stato;	// Vettore degli stati

} task_monitor;

void wait_monitor(task_monitor* m, int task_index);

//Inizializzazione delle variabili presenti nella struttura e allocazione della memoria
void init_monitor(task_monitor* m, int num_tasks);

//Setter dello stato all'indice task_index, utilizza mutex del monitor per rendere le operazioni esclusive
void set_stato(task_monitor* m, stato_task s, int task_index);

//Getter dello stato all'indice task_index, utilizza mutex del monitor per rendere le operazioni esclusive
stato_task get_stato(task_monitor* m, int task_index);

//Deallocazione della memoria monitor
void monitor_destroy(task_monitor* m, int num_tasks);

#endif
